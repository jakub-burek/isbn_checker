#pragma once
#if !defined(__C_FileWriter__)
#define __C_FileWriter__

#include "IFileWriter.h"
#include <fstream>

class CFileWriter : public IFileWriter {
public:
	CFileWriter();
	CFileWriter(std::string);
	~CFileWriter();
	virtual bool openWriter(std::string);
	virtual bool closeWriter();
	virtual bool writeLine(std::string);
	virtual bool isOpen();

private:
	std::ofstream outputFileStream;
};

#endif //__C_FileWriter__