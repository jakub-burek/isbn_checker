#pragma once
#if !defined(__C_ISBNAlgorythm__)
#define __C_ISBNAlgorythm__

#include <vector>

#include "IISBNAlgorythm.h"
#include "AssemblerDLL.h"

class CISBNAlgorythm : public IISBNAlgorythm {
public:
	CISBNAlgorythm() = default;
	~CISBNAlgorythm() = default;
	virtual std::string validateLineCpp(std::string);
	virtual std::string validateLineASM(std::string);

private:
    unsigned int calculateChecksumNumber(std::string);
	std::string retriveDigitsFromDashedNumber(unsigned int &, std::string);
};

#endif //__C_ISBNAlgorythm__