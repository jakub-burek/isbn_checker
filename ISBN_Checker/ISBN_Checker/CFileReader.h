#pragma once
#if !defined(__C_FileReader__)
#define __C_FileReader__

#include "IFileReader.h"
#include <fstream>

class CFileReader : public IFileReader {
public:
	CFileReader();
	CFileReader(std::string);
	~CFileReader();
	virtual bool openReader(std::string);
	virtual bool closeReader();
	virtual std::string readLine();
	virtual bool hasNext();
	virtual bool isOpen();
	virtual unsigned int countInputLines();

private:
	std::ifstream inputFileStream;
};

#endif //__C_FileReader__