#include "CFileReader.h"

CFileReader::CFileReader()
{

}

CFileReader::CFileReader(std::string path)
{
	inputFileStream.open(path, std::fstream::in);
}

CFileReader::~CFileReader()
{
	if(inputFileStream.is_open())
		inputFileStream.close();
}

bool CFileReader::openReader(std::string path)
{
	inputFileStream.open(path, std::fstream::in);
	if (inputFileStream.good())
		return true;
	else
		return false;
}

bool CFileReader::closeReader()
{
	if (inputFileStream.is_open())
		inputFileStream.close();

	if (inputFileStream.is_open())
		return false;
	else
		return true;
}

std::string CFileReader::readLine()
{
	std::string inputString("");
	getline(inputFileStream, inputString);
	return inputString;
}

bool CFileReader::hasNext()
{
	if (!inputFileStream.is_open())
		return false;

	if (inputFileStream.eof())
		return false;
	else
		return true;
}

bool CFileReader::isOpen()
{
	if (inputFileStream.is_open())
		return true;
	else
		return false;
}

unsigned int CFileReader::countInputLines()
{
	if (!inputFileStream.is_open())
		return 0;

	unsigned int numberOfLines = 0;
	std::string data;
	while (std::getline(inputFileStream, data))
		++numberOfLines;

	inputFileStream.clear();
	inputFileStream.seekg(0, inputFileStream.beg);

	return numberOfLines;
}
