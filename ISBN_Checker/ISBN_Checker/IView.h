#pragma once
#if !defined(__I_View__)
#define __I_View__

class IView {
public:
	IView() = default;
	~IView() = default;
};

#endif //__I_View__