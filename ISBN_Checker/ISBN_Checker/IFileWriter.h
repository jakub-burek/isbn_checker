#pragma once
#if !defined(__I_FileWriter__)
#define __I_FileWriter__

#include <string>

class IFileWriter {
public:
	IFileWriter() = default;
	~IFileWriter() = default;
	virtual bool openWriter(std::string) = 0;
	virtual bool closeWriter() = 0;
	virtual bool writeLine(std::string) = 0;
	virtual bool isOpen() = 0;
};

#endif //__I_FileWriter__