#pragma once
#if !defined(__I_ISBNAlgorythm__)
#define __I_ISBNAlgorythm__

#include <string>

class IISBNAlgorythm{
public:
	IISBNAlgorythm() = default;
	~IISBNAlgorythm() = default;
	virtual std::string validateLineCpp(std::string) = 0;
	virtual std::string validateLineASM(std::string) = 0;
};

#endif //__I_ISBNAlgorythm__