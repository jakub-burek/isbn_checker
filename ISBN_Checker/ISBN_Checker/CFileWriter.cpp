#include "CFileWriter.h"

CFileWriter::CFileWriter()
{

}

CFileWriter::CFileWriter(std::string path)
{
	outputFileStream.open(path, std::fstream::out);
}

CFileWriter::~CFileWriter()
{
	if (outputFileStream.is_open())
		outputFileStream.close();
}

bool CFileWriter::openWriter(std::string path)
{
	outputFileStream.open(path, std::fstream::out);
	if (outputFileStream.is_open())
		return true;
	else
		return false;
}

bool CFileWriter::closeWriter()
{
	if (outputFileStream.is_open())
		outputFileStream.close();

	if (outputFileStream.is_open())
		return false;
	else
		return true;
}

bool CFileWriter::writeLine(std::string outputString)
{
	if (outputFileStream.is_open())
	{
		outputFileStream << outputString << std::endl;
		return true;
	}
	else
		return false;
}

bool CFileWriter::isOpen()
{
	if (outputFileStream.is_open())
		return true;
	else
		return false;
}