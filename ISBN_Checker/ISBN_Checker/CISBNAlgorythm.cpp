#include "CISBNAlgorythm.h"

std::string CISBNAlgorythm::validateLineCpp(std::string dataString)
{
	unsigned int lineIterator = 0;

	while (lineIterator < dataString.size() && !isdigit((unsigned char)dataString[lineIterator]))
		++lineIterator;
	if (lineIterator >= dataString.size())
		return "";

	unsigned int wordPositionIterator = lineIterator;
	while (lineIterator < dataString.size() && (isdigit((unsigned char)dataString[lineIterator]) || dataString[lineIterator] == '-'))
		++lineIterator;

	std::string dashedNumberString = dataString.substr(wordPositionIterator, lineIterator - wordPositionIterator);

	unsigned int digitCount = 0;
	unsigned int dashCount = 0;
	for (unsigned int i = 0; i < dashedNumberString.size(); ++i)
	{
		if (isdigit((unsigned char)dashedNumberString[i]))
			++digitCount;
		if (dashedNumberString[i] == '-')
			++dashCount;
	}
	if (digitCount != 13)
		return dashedNumberString + " /wrong_digit_count";
	if (dashCount != 4)
		return dashedNumberString + " /wrong_dash_count";

	unsigned int dashedNumberIterator = 0;
	std::vector<std::string> numberSegments;
	while (dashedNumberIterator < dashedNumberString.size())
		numberSegments.push_back(retriveDigitsFromDashedNumber(dashedNumberIterator, dashedNumberString));

	if (numberSegments.size() != 5)
		return dashedNumberString + " /wrong_segments_count";

	if (numberSegments.at(0).size() != 3)
		return dashedNumberString + " /wrong_segment_1_size";
	if (numberSegments.at(1).size() < 1 || numberSegments.at(1).size() > 5)
		return dashedNumberString + " /wrong_segment_2_size";
	if (numberSegments.at(2).size() < 1)
		return dashedNumberString + " /wrong_segment_3_size";
	if (numberSegments.at(3).size() < 1)
		return dashedNumberString + " /wrong_segment_4_size";
	if (numberSegments.at(4).size() != 1)
		return dashedNumberString + " /wrong_segment_5_size";

	unsigned int sum = calculateChecksumNumber(dashedNumberString);

	if (atoi(numberSegments.at(4).c_str()) != sum)
		return dashedNumberString + " /wrong_control_sum /correct_control_sum:" + std::to_string(sum);

	return dashedNumberString + " /correct_code";
}

std::string CISBNAlgorythm::validateLineASM(std::string dataString)
{
	char* dataArray = _strdup(dataString.c_str());
	if (dataArray == NULL)
		return std::string("There is no code");
	char outputArray[100] = "";
	validateCharacterArray(dataArray, outputArray);
	return std::string(outputArray);
}

unsigned int CISBNAlgorythm::calculateChecksumNumber(std::string dashedNumberString)
{
	unsigned int sum = 0;
	unsigned int multiplier = 1;
	for (unsigned int i = 0; i < dashedNumberString.size() - 1; ++i)
	{
		if (isdigit((unsigned char)dashedNumberString[i]))
		{
			std::string digit = dashedNumberString.substr(i, 1);
			sum = sum + atoi(digit.c_str()) * multiplier;
			if (multiplier == 1)
				multiplier = 3;
			else if (multiplier == 3)
				multiplier = 1;
		}
	}

	sum = sum % 10;
	sum = 10 - sum;
	sum = sum % 10;
	return sum;
}

std::string CISBNAlgorythm::retriveDigitsFromDashedNumber(unsigned int & iterator, std::string dashedNumber)
{
	unsigned int beginingIterator = iterator;
	while (iterator < dashedNumber.size() && isdigit((unsigned char)dashedNumber[iterator]))
		++iterator;

	std::string dashedNumberString = dashedNumber.substr(beginingIterator, iterator - beginingIterator);

	if (iterator < dashedNumber.size() - 1)
		iterator++;

	return dashedNumberString;
}
