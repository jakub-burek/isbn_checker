#pragma once
#if !defined(__Form__)
#define __Form__

#include <iostream>
#include <memory>
#include <chrono>
#include <msclr\marshal_cppstd.h>

#include "CISBNAlgorythm.h"
#include "CFileReader.h"
#include "CFileWriter.h"

namespace ISBNWindowsForm {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for MyForm
	/// </summary>
	public ref class ISBNForm : public System::Windows::Forms::Form
	{
	public:
		ISBNForm(void)
		{
			InitializeComponent();
			fileReader = new CFileReader();
			fileWriter = new CFileWriter();
			ISBNAlgorythm = new CISBNAlgorythm();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~ISBNForm()
		{
			if (components)
			{
				delete components;
			}
			if (fileReader != nullptr)
			{
				delete fileReader;
				fileReader = nullptr;
			}
			if (fileWriter != nullptr)
			{
				delete fileWriter;
				fileWriter = nullptr;
			}
			if (ISBNAlgorythm != nullptr)
			{
				delete ISBNAlgorythm;
				ISBNAlgorythm = nullptr;
			}
		}

	private: System::Windows::Forms::Button^  asmButton;
	private: System::Windows::Forms::TextBox^  InputFileTextBox;
	private: System::Windows::Forms::Label^  InputLabel;
	private: System::Windows::Forms::Label^  OutputLabel;
	private: System::Windows::Forms::TextBox^  OutputFileTextBox;
	private: System::Windows::Forms::Button^  cppButton;
	private: IFileReader* fileReader;
	private: IFileWriter* fileWriter;
	private: IISBNAlgorythm* ISBNAlgorythm;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->asmButton = (gcnew System::Windows::Forms::Button());
			this->InputFileTextBox = (gcnew System::Windows::Forms::TextBox());
			this->InputLabel = (gcnew System::Windows::Forms::Label());
			this->OutputLabel = (gcnew System::Windows::Forms::Label());
			this->OutputFileTextBox = (gcnew System::Windows::Forms::TextBox());
			this->cppButton = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// asmButton
			// 
			this->asmButton->Location = System::Drawing::Point(231, 124);
			this->asmButton->Name = L"asmButton";
			this->asmButton->Size = System::Drawing::Size(100, 50);
			this->asmButton->TabIndex = 1;
			this->asmButton->Text = L"Run ASM";
			this->asmButton->UseVisualStyleBackColor = true;
			this->asmButton->Click += gcnew System::EventHandler(this, &ISBNForm::asmButton_Click);
			// 
			// InputFileTextBox
			// 
			this->InputFileTextBox->Location = System::Drawing::Point(12, 29);
			this->InputFileTextBox->Name = L"InputFileTextBox";
			this->InputFileTextBox->Size = System::Drawing::Size(394, 22);
			this->InputFileTextBox->TabIndex = 2;
			// 
			// InputLabel
			// 
			this->InputLabel->AutoSize = true;
			this->InputLabel->Location = System::Drawing::Point(12, 9);
			this->InputLabel->Name = L"InputLabel";
			this->InputLabel->Size = System::Drawing::Size(148, 17);
			this->InputLabel->TabIndex = 3;
			this->InputLabel->Text = L"Enter Input File Name:";
			// 
			// OutputLabel
			// 
			this->OutputLabel->AutoSize = true;
			this->OutputLabel->Location = System::Drawing::Point(12, 54);
			this->OutputLabel->Name = L"OutputLabel";
			this->OutputLabel->Size = System::Drawing::Size(156, 17);
			this->OutputLabel->TabIndex = 4;
			this->OutputLabel->Text = L"Enter Output File Name";
			// 
			// OutputFileTextBox
			// 
			this->OutputFileTextBox->Location = System::Drawing::Point(12, 74);
			this->OutputFileTextBox->Name = L"OutputFileTextBox";
			this->OutputFileTextBox->Size = System::Drawing::Size(394, 22);
			this->OutputFileTextBox->TabIndex = 5;
			// 
			// cppButton
			// 
			this->cppButton->Location = System::Drawing::Point(68, 124);
			this->cppButton->Name = L"cppButton";
			this->cppButton->Size = System::Drawing::Size(100, 50);
			this->cppButton->TabIndex = 0;
			this->cppButton->Text = L"Run C++";
			this->cppButton->UseVisualStyleBackColor = true;
			this->cppButton->Click += gcnew System::EventHandler(this, &ISBNForm::cppButton_Click);
			// 
			// ISBNForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(8, 16);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(419, 198);
			this->Controls->Add(this->OutputFileTextBox);
			this->Controls->Add(this->OutputLabel);
			this->Controls->Add(this->InputLabel);
			this->Controls->Add(this->InputFileTextBox);
			this->Controls->Add(this->asmButton);
			this->Controls->Add(this->cppButton);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
			this->MaximizeBox = false;
			this->Name = L"ISBNForm";
			this->Text = L"ISBN Checker";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void cppButton_Click(System::Object^  sender, System::EventArgs^  e) {
		msclr::interop::marshal_context context;
		std::string inputFileName = context.marshal_as<std::string>(InputFileTextBox->Text);
		std::string outputFileName = context.marshal_as<std::string>(OutputFileTextBox->Text);

		fileReader->openReader(inputFileName);
		fileWriter->openWriter(outputFileName);

		if (!fileReader->isOpen() || !fileWriter->isOpen())
		{
			MessageBox::Show("Type in appropriate file names.", "ISBN Checker", MessageBoxButtons::OK, MessageBoxIcon::Error);
			return;
		}
			
		
		unsigned int countLines = fileReader->countInputLines();
		std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();

		while (fileReader->hasNext())
		{
			std::string inputData = fileReader->readLine();
			std::string outputData = ISBNAlgorythm->validateLineCpp(inputData);
			if (!(outputData == ""))
				fileWriter->writeLine(outputData);
		}

		std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
		long long duration = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
		MessageBox::Show("Cpp calculations are complete. Input file has: " + countLines + " lines. Execution time: " + duration + " microseconds", "ISBN Checker", MessageBoxButtons::OK, MessageBoxIcon::Asterisk);

		fileReader->closeReader();
		fileWriter->closeWriter();
	}

	private: System::Void asmButton_Click(System::Object^  sender, System::EventArgs^  e) {
		msclr::interop::marshal_context context;
		std::string inputFileName = context.marshal_as<std::string>(InputFileTextBox->Text);
		std::string outputFileName = context.marshal_as<std::string>(OutputFileTextBox->Text);

		fileReader->openReader(inputFileName);
		fileWriter->openWriter(outputFileName);

		if (!fileReader->isOpen() || !fileWriter->isOpen())
		{
			MessageBox::Show("Type in appropriate file names.", "ISBN Checker", MessageBoxButtons::OK, MessageBoxIcon::Error);
			return;
		}

		unsigned int countLines = fileReader->countInputLines();
		std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();

		while (fileReader->hasNext())
		{
			std::string inputData = fileReader->readLine();
			std::string outputData = ISBNAlgorythm->validateLineASM(inputData);
			if (!(outputData == ""))
				fileWriter->writeLine(outputData);
		}

		std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
		long long duration = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
		MessageBox::Show("ASM calculations are complete. Input file has: " + countLines + " lines. Execution time: " + duration + " microseconds", "ISBN Checker", MessageBoxButtons::OK, MessageBoxIcon::Asterisk);

		fileReader->closeReader();
		fileWriter->closeWriter();
	}
};
}
#endif //__Form__