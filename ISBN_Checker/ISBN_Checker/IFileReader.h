#pragma once
#if !defined(__I_FileReader__)
#define __I_FileReader__

#include <string>

class IFileReader {
public:
	IFileReader() = default;
	~IFileReader() = default;
	virtual bool openReader(std::string) = 0;
	virtual bool closeReader() = 0;
	virtual std::string readLine() = 0;
	virtual bool hasNext() = 0;
	virtual bool isOpen() = 0;
	virtual unsigned int countInputLines() = 0;
};

#endif //__I_FileReader__