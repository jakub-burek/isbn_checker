; **************************************************************************************************
;* Project, Assembly Languages																		*
;* Jakub Burek, group 3-4																			*
;* Gliwice, 2018																			*
; **************************************************************************************************
.386						; choosing architecture/command set
.model flat, c				; choosing memory and calling model

.const
EOFCharacter        BYTE 0	; constant representing end of character array
DashCharacter       BYTE 45	; constant representing dash character
BeforeZeroCharacter BYTE 47	; constant representing before-zero character
ZeroCharacter		BYTE 48 ; constant representing zero character
AfterNineCharacter  BYTE 58	; constant representing after-nine character
SpaceCharacter		BYTE 32 ; constant representing space character

.data
NoCodeMSG					DB '/There is no code in this line', 0FFH			; no code MSG
WrongLenghtMSG				DB '/This code is of inappropraite lenght', 0FFH	; wrong lenght MSG
WrongDigitCountMSG			DB '/This code has a wrong count of digits', 0FFH	; wrong digit count MSG
WrongFirstSegmentCountMSG	DB '/First segment has wrong digit count', 0FFH		; first segment wrong count MSG
WrongSecondSegmentCountMSG	DB '/Second segment has wrong digit count', 0FFH	; second segment wrong count MSG
WrongThirdSegmentCountMSG	DB '/Third segment has wrong digit count', 0FFH		; third segment wrong count MSG
WrongFourthSegmentCountMSG	DB '/Fourth segment has wrong digit count', 0FFH	; fourth segment wrong count MSG
WrongFifthSegmentCountMSG	DB '/Fifth segment has wrong digit count', 0FFH		; fifth segment wrong count MSG
WrongCheckSumDigitMSG		DB '/Wrong check sum digit, should be: ', 0FFH		; wrong check sum digit MSG
GoodCodeMSG					DB '/Code is appropriate', 0FFH						; code is appropriate MSG
EmptyCharArray				DB ' ', 0											; empty char array

.code

; ************************************************************************************************
; putISBNCodeToCharArray prototype, definition and description later

putISBNCodeToCharArray proto BeginingInputPointer: DWORD,  EndingInputPointer: DWORD, Message: DWORD, OutputPointer: DWORD, CheckSum: BYTE

; **************************************************************************************************
;* Procedure validateCharacterArrayASM looks for ISBN codes inside input character array and		*
;* reports results by writing a message to output character array									*
;*																									*
;* Input parameters:																				*
;* InputCharacterArray - pointer to char array, we look for ISBN code in this array					*
;* OutputCharArray	   - pointer to char array, we write output message in this array				*
;*																									*
;* Ouput parameter:																					*
;* there are no output parameters																	*
;*																									*
; **************************************************************************************************
validateCharacterArrayASM proc InputCharacterArray: DWORD, OutputCharArray: DWORD

LOCAL FirstNumberPosition: DWORD	; variable to hold position of the first number in the char array
LOCAL LastNumberPosition:  DWORD	; variable to hold position of the last number in the char array
LOCAL DashCount:		   BYTE 	; variable to hold dash count, should be 4 in the end
LOCAL FirstSegmentCount:   BYTE		; variable to hold first segment number count
LOCAL SecondSegmentCount:  BYTE		; variable to hold second segment number count
LOCAL ThirdSegmentCount:   BYTE		; variable to hold third segment number count
LOCAL FourthSegmentCount:  BYTE		; variable to hold fourth segment number count
LOCAL DigitSum:			   WORD		; variable to hold sum of ISBN code digits
LOCAL Multiplier:		   BYTE		; variable to hold multiplier needed to count ISBN control digit
LOCAL DigitSumChar:		   BYTE		; variable to hold sum charh of ISBN code

; ************************************************************************************************
; finding first number in InputCharacterArray

	mov		ebx, InputCharacterArray	; load InputCharacterArray address into EBX

FindFirstNumber:
    mov		al, [ebx]				; get a character 
    cmp		al, EOFCharacter		; compering character from string to our char
    je		EndOfLineNoCode			; if EOF character found jump
	mov		ah, al					; move character to accumulator al to check if number
	sub		ah, AfterNineCharacter	; checking first condition x < 58
	ja		CheckNextChar			; if condition not met look again
	mov		ah, al					; putting current char in ah
	sub		ah, BeforeZeroCharacter	; checking second condition x > 47
	jb		CheckNextChar			; if condition not met look again
	jmp		FirstNumberFound		; jump if first number found
 
EndOfLineNoCode:
	lea		eax, NoCodeMSG			; loading effective addres of message to eax
	lea		ebx, EmptyCharArray		; there is no code so we have to provide procedure with dummy message
	invoke 	putISBNCodeToCharArray, ebx, ebx, eax, OutputCharArray, -1	; invoking printing procedure
	ret								; return point

CheckNextChar:
     inc	ebx						; point to next byte
     jmp	FindFirstNumber			; compere next char is table

; ************************************************************************************************
; if first number is found look for the rest of the code

FirstNumberFound:
	mov		FirstNumberPosition, ebx; move address of the first number to the variable

FindNextNumber:
	inc		ebx						; move to next byte
	mov		al, [ebx]				; get a character 
    cmp		al, EOFCharacter		; compering character from string to our char
    je		EONumbersSomeCode		; if EOF character found jump
	cmp		al, DashCharacter		; if dash is found jump
	je		FindNextNumber			; look for next number/dash
	mov		ah, al					; move character to accumulator ah to check if number
	sub		ah, AfterNineCharacter	; checking first condition x < 58
	ja		EONumbersSomeCode		; if condition not met we check how many numbers found
	mov		ah, al					; putting current char in ah
	sub		ah, BeforeZeroCharacter	; checking first condition x > 47
	jb		EONumbersSomeCode		; if condition not met we check how many numbers found
	jmp		FindNextNumber			; look for next number/dash

; ************************************************************************************************
; if any chain of numbers and dashes found examine it

EONumbersSomeCode:
	mov		eax, ebx				; move address of (last character + 1)
	sub		eax, FirstNumberPosition; check lenght of potential ISBN code
	cmp		eax, 17					; if code is of good lenght check other things
	je		RightCodeLenght			; jump to next part
	sub		ebx, 1					; we need to move to character before first non-numerical character
	lea		eax, WrongLenghtMSG		; loading effective addres of message to eax
	invoke 	putISBNCodeToCharArray, FirstNumberPosition, ebx, eax, OutputCharArray, -1	; invoking printing procedure
	ret								; return point

RightCodeLenght:
	mov		eax, ebx				; if code is of right lenght we calculate address of last number
	sub		eax, 1					; we need to substract 1 from eax, because it is first address after last digit 
	mov		LastNumberPosition, eax	; we save calculated address in a variable
	mov		DashCount, 0			; initialize dash counter
	mov		ebx, FirstNumberPosition; move iterator to the beginning of the table

CheckNextNumber:
	cmp		ebx, LastNumberPosition	; check if we are in range
	je		CheckLastNumber			; check last number
	mov		al, [ebx]				; move character to al
	inc		ebx						; move to next byte
	cmp		al, DashCharacter		; check if character is dash
	je		CountDash				; if character is dash jump
	jmp		CheckNextNumber			; check next character
CountDash:
	inc		DashCount				; if character is dash increment DashCount
	jmp		CheckNextNumber			; check next character

CheckLastNumber:
	mov		al, [ebx]				; move character to al
	cmp		al, DashCharacter		; check if character is dash
	je		CountLastDash			; if it is a dash jump
	jmp		CheckCount				; go to results check
CountLastDash:
	inc DashCount					; if it is a dash increment

CheckCount:
	cmp		DashCount, 4			; check if there is correct dash count
	je		CorrectOverallCount		; if overall count is good go to the next phase
	lea		eax, WrongDigitCountMSG ; loading effective addres of message to eax
	invoke 	putISBNCodeToCharArray, FirstNumberPosition, LastNumberPosition, eax, OutputCharArray, -1 ; invoking printing procedure
	ret								; return point

; ************************************************************************************************
; now check how many numbers are in each segment

CorrectOverallCount:
	mov		ebx, FirstNumberPosition; moving beginning of array pointer to ebx
	mov		FirstSegmentCount, 0	; preparing variable to count how many digits are in first segment

CountFirstSegment:
	mov		al, [ebx]				; move character to al
	cmp		al, DashCharacter		; check if character is dash
	je		BeginCountSecondSegment	; if dash is found we count second segment numbers
	inc		FirstSegmentCount		; if not dash, then number, increase count
	inc		ebx						; go to next byte
	jmp		CountFirstSegment		; look for next number in this segment

BeginCountSecondSegment:
	cmp		FirstSegmentCount, 3	; check if first segment has 3 numbers
	jne		FirstCountError			; if not equal jump
	mov		SecondSegmentCount, 0	; prepare for counting second segment
	inc		ebx						; move to next byte

CountSecondSegment:
	mov		al, [ebx]				; move character to al
	cmp		al, DashCharacter		; check if character is dash
	je		BeginCountThirdSegment	; if dash is found we count second segment numbers
	inc		SecondSegmentCount		; if not dash, then number, increase count
	inc		ebx						; go to next byte
	jmp		CountSecondSegment		; look for next number in this segment

BeginCountThirdSegment:
	mov		ah, SecondSegmentCount	; move second segment count to eax
	sub		ah, 6					; check if second segment is not too long
	jnb		SecondCountError		; if too long jump
	mov		ah, SecondSegmentCount	; move second segment count to eax
	sub		ah, 1					; check if not too short
	jb		SecondCountError		; jump if too short
	mov		ThirdSegmentCount, 0	; prepare for counting third segment
	inc		ebx						; move to next byte

CountThirdSegment:
	mov		al, [ebx]				; move character to al
	cmp		al, DashCharacter		; check if character is dash
	je		BeginCountFourthSegment	; if dash is found we count fourth segment numbers
	inc		ThirdSegmentCount		; if not dash, then number, increase count
	inc		ebx						; go to next byte
	jmp		CountThirdSegment		; look for next number in this segment

BeginCountFourthSegment:
	mov		ah, ThirdSegmentCount	; move third segment count to eax
	sub		ah, 1					; check if not too short
	jb		ThirdCountError			; jump if too short
	mov		FourthSegmentCount, 0	; prepare for counting third segment
	inc		ebx						; move to next byte

CountFourthSegment:
	mov		al, [ebx]				; move character to al
	cmp		al, DashCharacter		; check if character is dash
	je		BeginCountFifthSegment	; if dash is found we count fourth segment numbers
	inc		FourthSegmentCount		; if not dash, then number, increase count
	inc		ebx						; go to next byte
	jmp		CountFourthSegment		; look for next number in this segment

BeginCountFifthSegment:
	mov		ah, FourthSegmentCount	; move third segment count to eax
	sub		ah, 1					; check if not too short
	jb		FourthCountError		; jump if too short
	inc		ebx						; move to next byte

CountFifthSegment:
	mov		al, [ebx]				; move character to al
	cmp		al, DashCharacter		; check if character is dash
	je		FifthCountError			; if last is dash it is error
	cmp		ebx, LastNumberPosition	; if it is not last character then this segment is bigger than it should be
	jne		FifthCountError			; if not last number jump
	jmp		CalculateControlDigit	; if segments are good calculate control digit

FirstCountError:
	lea		eax, WrongFirstSegmentCountMSG	; loading effective addres of message to eax
	invoke 	putISBNCodeToCharArray, FirstNumberPosition, LastNumberPosition, eax, OutputCharArray, -1	; invoking printing procedure
	ret								; return point

SecondCountError:
	lea		eax, WrongSecondSegmentCountMSG	; loading effective addres of message to eax
	invoke 	putISBNCodeToCharArray, FirstNumberPosition, LastNumberPosition, eax, OutputCharArray, -1	; invoking printing procedure
	ret								; return point

ThirdCountError:
	lea		eax, WrongThirdSegmentCountMSG	; loading effective addres of message to eax
	invoke 	putISBNCodeToCharArray, FirstNumberPosition, LastNumberPosition, eax, OutputCharArray, -1	; invoking printing procedure
	ret								; return point

FourthCountError:
	lea		eax, WrongFourthSegmentCountMSG	; loading effective addres of message to eax
	invoke 	putISBNCodeToCharArray, FirstNumberPosition, LastNumberPosition, eax, OutputCharArray, -1	; invoking printing procedure
	ret								; return point

FifthCountError:
	lea		eax, WrongFifthSegmentCountMSG	; loading effective addres of message to eax
	invoke 	putISBNCodeToCharArray, FirstNumberPosition, LastNumberPosition, eax, OutputCharArray, -1	; invoking printing procedure
	ret								; return point

; ************************************************************************************************
; now calculate control digit

CalculateControlDigit:
	mov		ebx, FirstNumberPosition; moving beginning of array pointer to ebx
	mov		DigitSum, 0				; preparing variable to hold digit sum
	mov		Multiplier, 1			; preparing variable holding digit multiplier

ControlNextChar:
	cmp		ebx, LastNumberPosition ; checking if summation is done
	je		SumIsReady				; if we summed all digits jump
	mov		al, [ebx]				; move character to al
	cmp		al, DashCharacter		; check if character is dash
	je		GoToNextChar			; if dash, we cannot add it to sum
	sub		al, ZeroCharacter		; change ASCII code to digit
	mul		Multiplier				; multiply al by multiplier, result in ax
	add		DigitSum, ax			; add digit to sum
	cmp		Multiplier, 1			; changing multiplier
	jne		ChangeToOne				; if multiplier is not 1, change it to 1
	mov		Multiplier, 3			; if multiplier is 1, change it to 3
	jmp		GoToNextChar			; jump to check next digit

ChangeToOne:
	mov		Multiplier, 1			; changing Multiplier to 1

GoToNextChar:
	inc		ebx						; move to next byte
	jmp		ControlNextChar			; check next char

SumIsReady:
	mov		dx, 0					; preparing dx for holding modulo result
	mov		ax, DigitSum			; moving digit sum to ax
	mov		bx, 10					; moving devider, 10, to bx
	div		bx						; divides ax by bx, dx is rest and ax is output
	mov		ax, 10					; moving 10 to ax
	sub		ax, dx					; substracting dx from ax
	mov		dx, 0					; preparing dx for holding modulo rest
	div		bx						; divides ax by bx, dx is rest and ax is output
	mov		DigitSum, dx			; moving result, control digit, to variable
 	mov		ebx, LastNumberPosition	; moving control digit address to ebx
	mov		ax, [ebx]				; moving control digit char to ax
	shl		ax, 8					; adjusting 8-bit to 16-bit value
	shr		ax, 8					; adjusting 8-bit to 16-bit value
	sub		ax, '0'					; changing ASCII code to digit
	cmp		DigitSum, ax			; comparing control digit with calculated control sum
	jne		ControlDigitError		; if not equal jumo to report error
	lea		eax, GoodCodeMSG		; loading effective addres of message to eax
	invoke 	putISBNCodeToCharArray, FirstNumberPosition, LastNumberPosition, eax, OutputCharArray, -1	; invoking printing procedure
	ret								; return point

ControlDigitError:
	add		DigitSum, '0'				; making char out of digit
	mov		ax, DigitSum 				; moving sum char to bx
	mov		DigitSumChar, al			; moving char to variable
	lea		ebx, WrongCheckSumDigitMSG	; loading effective addres of message to eax
	invoke 	putISBNCodeToCharArray, FirstNumberPosition, LastNumberPosition, ebx, OutputCharArray, DigitSumChar	; invoking printing procedure
	ret								; return point

validateCharacterArrayASM endp

; **************************************************************************************************
;* Procedure putISBNCodeToCharArray prints message to character array								*
;*																									*
;* Input parameters:																				*
;* BeginingInputPointer - pointer to char array, to the first character of ISBN code				*
;* EndingInputPointer   - pointer to char array, to the last character of ISBN code					*
;* Message				- pointer to char array, to the first character in message					*
;* OutputPointer		- pointer to char array, to the first character of empty char				*
;* CheckSum				- character representing check sum digit, if not than -1					*
;*																									*
;* Ouput parameter:																					*
;* there are no output parameters																	*
;*																									*
; **************************************************************************************************
putISBNCodeToCharArray proc BeginingInputPointer: DWORD,  EndingInputPointer: DWORD, Message: DWORD, OutputPointer: DWORD, CheckSum: BYTE
	mov		ebx, BeginingInputPointer	; moving begining of ISBN code address to ebx
	mov		ecx, OutputPointer			; moving begining of output table to ecx

FirstPrintLoop:
	cmp		ebx, EndingInputPointer		; check if end of ISBN code
	je		MoveToPrintMSG				; if end of code than print MSG
	mov		al, [ebx]					; moving char to al from ISBN code
	mov		[ecx], al					; moving char from al to output char array
	inc		ebx							; moving to next byte
	inc		ecx							; moving to next byte
	jmp		FirstPrintLoop				; printing next char of ISBN code

MoveToPrintMSG:
	mov		al, [ebx]					; moving last ISBN char to al from ISBN code
	mov		[ecx], al					; moving char from al to output char array
	inc		ecx							; moving to next byte
	mov		al, SpaceCharacter			; moving space char into al
	mov		[ecx], al					; putting space into array
	inc		ecx							; moving to next byte
	mov		ebx, Message				; preparing for MSG print

SecondPrintLoop:
	mov		al, [ebx]					; moving char from MSG to al
	cmp		al, 0FFH					; checking if it is end of MSG
	je		EndMSGPrint					; end printing MSG
	mov		[ecx], al					; moving char from al to output char array
	inc		ebx							; moving to next byte
	inc		ecx							; moving to next byte
	jmp		SecondPrintLoop				; printing next MSG char

EndMSGPrint:
	mov		al, CheckSum
	cmp		al, -1
	je		EndOfPrint
	mov		[ecx], al
EndofPrint:
	ret

putISBNCodeToCharArray endp

end