// AssemblerDLL.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "AssemblerDLL.h"

extern "C" int validateCharacterArrayASM(char*, char*);

void validateCharacterArray(char* InputCharacterArray, char* OuputCharacterArray)
{
	validateCharacterArrayASM(InputCharacterArray, OuputCharacterArray);
}